<?php

namespace App\Form;

use App\Entity\Upload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      //Construit mon formulaire pour upload mes fichier
        $builder
            //Le premier champs pour selectionner le premier fichier a upload
            ->add('name', FileType::class, array(
                'label' => 'Choisissez votre fichier'
    ))
            //Le deuxiemme champs pour selectionner le deuxiemme fichier a upload
        ->add('nom', FileType::class, array(
            'label' => 'Choisissez votre fichier'
        ))
    ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Upload::class,
        ]);
    }
}
