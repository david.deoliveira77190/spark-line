<?php
namespace App\Controller;

use App\Entity\Upload;
use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class UploadController extends AbstractController
{
  /**
   * @Route("/upload", name="upload")
   * @param Request $request
   * @return RedirectResponse|Response
   */
public function uploadFile(Request $request){
  //initie la classe Upload
  $upload = new Upload();
  //crée le formulaire
  $form =$this->createForm(UploadType::class, $upload);

  $form->handleRequest($request);
  //si on envoye le formulaire et qu'il est valide
  if ($form->isSubmitted() && $form->isValid()){
    //prend le nom de $file
      $file = $upload->getName();
      //on lui donnée le même nom que le fichier reçu
      $fileName = $file->getClientOriginalName();
      //on envoie le fichier $filename dans le dossier upload de l'app
      $file->move($this->getParameter('upload_directory'),$fileName);
      $upload->setName($fileName);

      $file2 = $upload->getNom();
      $fileName2 =$file2->getClientOriginalName();
      $file2->move($this->getParameter('upload_directory'),$fileName2);
      $upload->setNom($fileName2);

    return $this->redirectToRoute('acceuil');
  }

  return $this->render('/Site/index.html.twig', array(
      'form' => $form->createView(),
  ));
}

}