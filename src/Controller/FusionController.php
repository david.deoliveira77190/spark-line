<?php

namespace App\Controller;

use League\Csv\CannotInsertRecord;
use League\Csv\Exception;
use SplTempFileObject;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use League\Csv\Reader;
use League\Csv\Writer;

class FusionController extends AbstractController
{
  /**
   * @Route ("/fusion", name="fusionSeq")
   * @throws CannotInsertRecord
   * @throws Exception
   */
  public function fusionCsvSeq()
  {

    $header = [
      'Gender', 'Title', 'GivenName', 'Surname', 'StreetAddress', 'City', 'StateFull', 'ZipCode', 'CountryFull', 'EmailAddress', 'TelephoneNumber',
      'Birthday', 'CCType', 'CCNumber', 'CVV2', 'CCExpires', 'Vehicle', 'Kilograms', 'FeetInches', 'Latitude', 'Longitude'
    ];
    //initie un fichier temporaire ou ecrire le fusion
    $fusion = Writer::createFromFileObject(new SplTempFileObject());
    $fusion->insertOne($header);

    //ouvret et lit un fichier csv
    $handle = Reader::createFromPath("uploads/small-french-data.csv", "r");
    $handle->setHeaderOffset(0);

    //Pour chaque ligne dans le fichier je l'insert dans le fichier temporaire
    foreach ($handle as $data) {
      $content = [];
      foreach ($header as $column) {
        $content[] = $data[$column];
      }
      $fusion->insertOne($content);
    }

    //ouvert le deuxiemme fichier csv
    $hand = Reader::createFromPath("uploads/small-german-data.csv", "r");
    $hand->setHeaderOffset(0);

    //Pour chaque ligne dans le fichier je l'insert dans le fichier temporaire
    foreach ($hand as $data) {
      $content = [];
      foreach ($header as $column) {
        $content[] = $data[$column];
      }
      $fusion->insertOne($content);
    }
    

    //return le fichier temporaire avec les lignes de chaque fichier
    $fusion->output('fusion-sequentiel.csv');
    die;
    //die is necessary according to "https://csv.thephpleague.com/9.0/"
  }

  /**
   * @Route ("/fus", name="fusionEnt")
   * @throws Exception
   */
  public function fusionCsvEnt()
  {

    $header = [
      'Gender', 'Title', 'GivenName', 'Surname', 'StreetAddress', 'City', 'StateFull', 'ZipCode', 'CountryFull', 'EmailAddress', 'TelephoneNumber',
      'Birthday', 'CCType', 'CCNumber', 'CVV2', 'CCExpires', 'Vehicle', 'Kilograms', 'FeetInches', 'Latitude', 'Longitude'
    ];

    //initie un fichier temporaire ou ecrire le fusion
    $fusion = Writer::createFromFileObject(new SplTempFileObject());
    $fusion->insertOne($header);

    //ouvret et lit un fichier csv
    $handle = Reader::createFromPath("uploads/small-french-data.csv", "r");
    $handle->setHeaderOffset(0);

    $tab = [];

    //Pour chaque ligne dans le fichier je l'insert dans le tableau
    foreach ($handle as $data) {
      $content = [];
      foreach ($header as $column) {
        $content[] = $data[$column];
      }
      $tab[] = $content;
    }

    //ouvret et lit un fichier csv
    $hand = Reader::createFromPath("uploads/small-german-data.csv", "r");
    $hand->setHeaderOffset(0);

    $tabbis = [];

    //Pour chaque ligne dans le fichier je l'insert dans le tableau
    foreach ($hand as $data) {
      $content = [];
      foreach ($header as $column) {
        $content[] = $data[$column];
      }
      $tabbis[] = $content;
    }

    //calcul le nombre de ligne dans les tableaux
    $total = count($tab);
    $totalbis = count($tabbis);

    $i = 0;
    while ($i < $total and $i < $totalbis) {
      if ($tab[$i]) {
        //insert la ligne $i du cont$contentleau dans le fichier
        $fusion->insertOne($tab[$i]);
      }
      if ($tabbis[$i]) {
        $fusion->insertOne($tabbis[$i]);
      }
      $i++;
    }
    $x = 0;
    if ($totalbis < $total) {
      $diff = $total - $totalbis;
      while ($x < $diff) {
        if ($tab[$i]) {
          $fusion->insertOne($tab[$i]);
        }
        $i++;
        $x++;
      }
    } elseif ($total < $totalbis) {
      $diff = $totalbis - $total;
      while ($x < $diff) {
        if ($tabbis[$i]) {
          $fusion->insertOne($tabbis[$i]);
        }
        $i++;
        $x++;
      }
    }
    //return le fichier temporaire avec les lignes de chaque fichier
    $fusion->output('fusion-entrelaces.csv');
    die;
    //die is necessary according to "https://csv.thephpleague.com/9.0/"
  }
}
