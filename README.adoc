= Spark-Line

Niveau : Deuxième année de BTS SIO SLAM

== Présentation
Mission de développement d’une application métier (exploitation de documents CSV),
de préférence sous la forme d’une application web à base de framework.

=== Contexte

La société de service dans laquelle vous travaillez a comme client l’entreprise SPARK-LINE, createur de ligne de vêtements.
Le project est diviser en 2 parties, elles seront détaillées plus loin.

=== Objectif
Au cours de ce projet nous avions plusieurs objectifs :

* Lecture de fichiers csv
* Fusion de deux fichiers au format csv
* Gestion de l'upload et du download de fichier

== Partie 1

Pour cette première partie de mission, l'objectif est de constituer un fichier de client unique *french-german-client.csv*
à partir de deux fichiers d'exportation des clients Français *french-client.csv* et Allemand *german-client.csv*
transmis par le gestionnaire.

=== Fusions

Tout d'abord nous avons réalisé les 2 méthodes permettant de lire nos fichiers et de mettre leur contenu dans un tableau.

Pour le premier fichier : `read_csv_Fr()`

[, php]
----
public static function read_csv_Fr($csv)
    {
        $filenameFrench = $csv;
        $frenchCsv = fopen($filenameFrench, 'r'); <1>
        while ($frenchTab[] = fgetcsv($frenchCsv, 1024, ";")) ; <2>
        fclose($frenchCsv); <3>
        return $frenchTab; <4>
    }
----
<1> On ouvre notre fichier csv contenu dans la variable $filenameFrench et on l'attribut à la variable $frenchCsv
<2> On mets les éléments contenu dans $frenchCsv dans un tableau $frenchTab[]
<3> On ferme $frenchCsv
<4> On retourne le tableau $frenchTab

Pour le second fichier : `read_csv_Ger()`

[, php]
----
 public static function read_csv_Ger($csv){
        $filenameGerman = $csv;
        $germanCsv = fopen($filenameGerman, 'r');
        while ($germanTab[] = fgetcsv($germanCsv, 1024, ";")) ;
        fclose($germanCsv);
        return $germanTab;
    }
----

Ici même chose mais pour le second fichier

=== Algorithmes des fusions
Nous allons passer  à nos 2 deux algotithmes de fusion.

=== La Fusion Sequentielle
[, php]
----

  public static function sequentialMerge($frenchTab, $germanTab){
            $csvModel = '/download/small-data-empty.csv';
            $emptyFile = fopen($csvModel, 'w');
----
Tout d'abord on défini une méthode, `sequentialmerge` et lui attribut 2 paramêtres : `$frenchTab` et `$germanTab`, les tableaux associatifs avec les données des 2 fichiers csv.
Une variable, `$csvModel` contient un fichier csv vierge.
On associe à la variable `$emptyFile` l'ouverture de la variable `$csvModel`


[, php]
----
            foreach ($frenchTab as $value) {
                if ($value) {
                    fputcsv($emptyFile, $value, ',');
                }
            }
            foreach ($germanTab as $key => $value) {
                if ($key != 0) {
                if ($value) {

                    fputcsv($emptyFile, $value, ',');
                }
            }
            }
            fclose($emptyFile);

        }
----
On a ici l'algorithme : On fait un foreach de `$frenchTab` et si on a une `$value` on la mets dans le `$emptyFile`
et on refait la même chose pour le `$germanTab`. De cette manière, il y aura d'abord toutes les données de `$frenchTab` puis toutes celles de `$germanTab` dans le nouveau fichier.

Résultat en image :

image::images/sequential.png[]

=== La Fusion Entrelacé

[, php]
----

public static function interlacedMerge($frenchTab, $germanTab){
$filenameEmpty = '/download/small-data-empty.csv';
$emptyFile = fopen($filenameEmpty, 'w');

            $countTabFR = count($frenchTab) - 1;
            $countTabGER = count($germanTab) - 1;
            $countFR = 1;
            $countGER = 1;
            $count = 0;
            $difTab = 0;
            if ($countTabFR >= $countTabGER) {
                $difTab = $countTabFR - $countTabGER;
                fputcsv($emptyFile, $frenchTab[0], ',');
                while ($count < $countTabGER && $countGER < ($countTabGER)) {
                    fputcsv($emptyFile, $frenchTab[$countFR], ',');
                    fputcsv($emptyFile, $germanTab[$countGER], ',');
                    $countFR++;
                    $countGER++;
                    $count++;
                }
                while ($difTab !== 0) {
                    fputcsv($emptyFile, $frenchTab[$countFR], ',');
                    $difTab--;
                    $countFR++;
                }
            } else {
                $difTab = $countTabGER - $countTabFR;
                fputcsv($emptyFile, $germanTab[0], ',');
                while ($count < $countTabFR && $countFR < ($countTabFR)) {
                    fputcsv($emptyFile, $germanTab[$countGER], ',');
                    fputcsv($emptyFile, $frenchTab[$countFR], ',');
                    $countFR++;
                    $countGER++;
                    $count++;
                }
                while ($difTab !== 0) {
                    fputcsv($emptyFile, $germanTab[$countGER], ',');
                    $difTab--;
                    $countGER++;
                }
            }
----
Enfin voici l'algorithme de la fusion entrelacée qui permet d'avoir un tri qui alterne entre 1 client français et 1 client allemand et d'afficher le reste des clients lorsque que l'un des fichier n'a plus de valeur à ajouter.

Résultat en image :

image::images/interlaced.png[]

=== Création du formulaire d'upload de fichier

Pour déposer nos 2 fichiers csv, il faut un formulaire capable de les récuperer.

Nous avons donc créer la méthode `buildForm` dans la classe `CsvFormType`.

[, php]
----
public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('csvFile1', FileType::class, [ <1>
                'label' => 'CSV File 1',
                'mapped' => false,
                'required' => false])
            ->add('csvFile2', FileType::class, [ <2>
                'label' => 'CSV File 2',
                'mapped' => false,
                'required' => false,])
            ->add('isAttending', ChoiceType::class, [ <3>
                'label' => 'Choose Merge',
                'choices'  => [
                    'Sequential' => "Sequential",
                    'Interlaced' => "Interlaced",

                ],
            ]);

    }
----
Dans une variable `$builder`` nous avons 3 entrées :

<1> ajoute le premier fichier
<2> ajoute le second fichier
<3> choisir la fusion  à effectuer : ``Sequentielle`` ou ``Entrelacé``

=== MainController

La classe `MainController` est le noyau dur qui permet de lier tout nos éléments.

Tout d'abord une méthode `main` atteignable par la route `/main` qui correspond à l'écran d'accueil de notre application rendu dans `main.html.twig`.

[,php]
----
    /**
     * @Route("/main", name="main")
     */
    public function index()
    {
        return $this->render('main/main.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
----

image::images/main.png[]

Comme on peut le voir nous avons un bouton de connexion, celui permet d'accèder aux fusions si l'utilisateur enregistrer est administrateur.

Lorsque l'on est connecter en tant qu'administrateur, le bouton pour accèder au fusion apparaît

image::images/admin.png[]

Voici le *main.html.twig*

[,php]
----
{% extends 'base.html.twig' %}

{% block title %}Hello AccueilController!{% endblock %}

{% block body %}
<style>
    .example-wrapper { margin: 1em auto; max-width: 800px; width: 95%; font: 18px/1.5 sans-serif; }
    .example-wrapper code { background: #F5F5F5; padding: 2px 6px; }
</style>

<div class="example-wrapper">
    <h1>Hello {{ controller_name }}! ✅</h1>


    <ul>
        {% if app.user %}
        <div class="mb-3">
            You are logged in as {{ app.user.username }}  <a href="{{ path("app_logout") }}" style="border-radius: 50px" class="ml-2 btn btn-danger">LOGOUT</a>
        </div>
            <a href="{{ path("merge_main") }}" style="border-radius: 50px" class="mr-5 btn btn-primary">Merge</a>
        {% else %}
        <a href="{{ path("app_login") }}" style="border-radius: 50px" class="mr-5 btn btn-warning">CONNEXION</a>
        {% endif %}


    </ul>

</div>
{% endblock %}

----

Ensuite nous avons la méthode `mainMerge` atteignable par la route /merge. Cela nous amène au formulaire.

[,php]
----
    /**
     * @Route("/merge", name="merge_main")
     */
        public function mainMerge(Request $request)
        {
            $csvForm = $this->CreateForm(CsvFormType::class); <1>
            $csvForm->handleRequest($request);

            if ($csvForm->isSubmitted() && $csvForm->isValid()) { <2>
                $frenchTab = Merge::read_csv_Fr($csvForm->get("csvFile1")->getData());
                $germanTab = Merge::read_csv_Ger($csvForm->get("csvFile2")->getData());
----

<1> Tout d'abord on créer le formulaire `CsvFormType` que l'on a paramètré plus tôt dans la variable `$csvForm`.
<2> Ensuite on pose la condition que si le formulaire est rempli et valide, on injecte les méthodes de lecture des fichiers créées plus tôt.

[,php]
----
 if ($csvForm->get("isAttending")->getData() == "Sequential") { <3>
                    Merge::sequentialMerge($frenchTab, $germanTab); <7>
                    $this->addFlash('success', 'Sequential Merge Success');

                }
                if ($csvForm->get("isAttending")->getData() == "Interlaced") { <4>
                    Merge::interlacedMerge($frenchTab, $germanTab); <8>
                    $this->addFlash('success', 'Interlaced Merge Success');

                }
                return $this->redirectToRoute("file_download"); <5>

            }
            return $this->render('/main/fusion.html.twig', [ <6>
                'formulaireCsv' => $csvForm->createView(),
            ]);
        }
----

<3> Ensuite nous choisissons notre fusion et si nous prenons la Sequentielle, c'est la condition <3>, sinon c'est la <4> qui s'effectue (Les algorithmes sont injecter depuis le Service `Merge` <7> et //8)
<5> Cliquer sur bouton *Valider* enregistre notre choix et nous renvoie sur la page `file_download` qui nous permettra de télécharger notre fichier issu de la fusion.
<6> On choisit d'affichier notre formulaire du la vue `fusion/html.twig`

image::images/merge.png[]

*Et voici le code de la vue `main/fusion.html.twig`*

[,php]
----
{% extends 'base.html.twig' %}

{% block body %}
    <a class="btn btn-warning mt-3 ml-3" href="{{ path("main") }}">Main</a>
<div     class="text-center mt-3">
    <h2> Merge Form</h2>

<div class="mx-auto container mt-5 " style="width: 500px">
    {{ form_start(formulaireCsv) }}

    <div class="form-control-file border text-center" style="background-color: #f6f0fa">
        {{ form_label(formulaireCsv.csvFile<1> }}
        <br>
        {{ form_widget(formulaireCsv.csvFile<1> }}
        {{ form_errors(formulaireCsv.csvFile<1> }}
    </div>
    <br>
    <div class="form-control-file border text-center" style="background-color: #f6f0fa">
        {{ form_label(formulaireCsv.csvFile<2> }}
        <br>
        {{ form_widget(formulaireCsv.csvFile<2> }}
        {{ form_errors(formulaireCsv.csvFile<2> }}
    </div>
    <br>
    <div class="form-control-file border text-center" style="background-color: #f6f0fa">
        {{ form_label(formulaireCsv.isAttending,) }}
        <br>
        {{ form_widget(formulaireCsv.isAttending) }}
        {{ form_errors(formulaireCsv.isAttending) }}
    </div>
    <br>

    <button type="submit"
            class="btn btn-primary mt-3 text-center">
        Valider
    </button>
</div>
</div>
    {{ form_end(formulaireCsv) }}

    {% endblock %}
----

La vue `download.html.twig` pour pouvoir télécharger le fichier après avoir cliquer sur *Valider*

image::images/download.png[]

Lorsque l'on clique sur *Télécharger* le téléchargement du fichier est lancé.

image::images/filedl.png[]

*Et voici le code de la vue `main/download.html.twig`*

[,php]
----
{% extends 'base.html.twig' %}

{% block body %}
    <a class="btn btn-warning mt-3 ml-3" href="{{ path("main") }}">Main</a>
    <div class="text-center">
    <h1>Voici votre fichier !</h1>

    <a class="btn btn-success" href="/download/small-data-empty.csv" download="small-data-empty.csv"> Télécharger</a>
    </div>
{% endblock %}
----

=== Test Unitaires

Pour conclure cette première partie, nous avons effectué 2 tests unitaires.

[,php]
----
class Test extends TestCase
{

    public function testPresenceDonneeTab1()
    {
        $tab = \App\Service\Merge::read_csv_Fr('\uploads\small-french-client.csv');
        $count = count($tab);
        $this->assertGreaterThanOrEqual(0, $count);
    }

    public function testPresenceDonneeTab2()
    {
        $tab = \App\Service\Merge::read_csv_Ger('\uploads\small-german-client.csv');
        $count = count($tab);
        $this->assertGreaterThanOrEqual(0, $count);
    }
}
----

Nos 2 méthodes *`testPresendeDonneeTab1`* et *`testPresendeDonneeTab2`* permettent de vérifier séparement si les fichiers transmis au formulaire contiennent des données.

On peut voir que les tests sont probants, nos fichiers contiennent bien des données.

image::images/testsUnitaires.png[]

== Partie 2

Malheureusement nous avons manquer de temps pour réaliser la partie 2.

== Conclusion

Cette mission permet de fusionner 2 fichiers au format csv en 1 seul, et ce en choisissant entre 2 types de fusion et de pouvoir télécharger ce dernier.
Nous avons manquer de temps pour effectuer la partie 2 mais satisfait du travail effectué sur la Partie 1. Cela a tout de même été enrichissant de travailler sur une mission dans une ambiance de travail d'entreprise.

Lien du dépot de référence : https://ocapuozzo.github.io/mission-etl-csv/#pr%C3%A9sentation
